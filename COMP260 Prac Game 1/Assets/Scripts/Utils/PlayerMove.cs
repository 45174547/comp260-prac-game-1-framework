﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
		
	public string Horizontal, Vertical;

	public float maxSpeed = 5.0f;

	public Vector2 move;

	// Update is called once per frame
	void Update () {
		// get the input values
		Vector2 direction;
		direction.x = Input.GetAxis (Horizontal);
		direction.y = Input.GetAxis (Vertical);

		// scale the velocity by the frame duration
		Vector2 velocity = direction * maxSpeed;

		// move the object
		transform.Translate(velocity * Time.deltaTime);
	}
}
